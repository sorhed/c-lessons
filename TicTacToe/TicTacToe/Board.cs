﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public class Board
    {
        private Cell[] cells;

        public Board() // nowa klasa 
        {
            cells = new Cell[9]; // tablica z 9 komórkami / tablica obiektów
								 // czemu nie możemy zastosować tablicy dwuwymiarowej? 
								 //int[,] tablica = new int[,] { { 1, 2 }, { 3, 4 }, { 5, 6 }, { 7, 8 } }
			int i = 0;

            for (int x = 1; x < 4; x++) // podwójna pętla
            {
                for (int y = 1; y < 4; y++)
                {              
                    Cell komorka = new Cell(x, y); // nowy obiekt "komórka" wypełniony dwoma pętlami (row, line)      
					cells[i++] = komorka;
					//Console.WriteLine(komorka.GetRow().ToString() + " row " + komorka.GetColumn().ToString() + " column" + " num of cell " + (i - 1).ToString());
                }            
            }
        }

		public void DrawBoard()
		{
			if (cells == null) return;

			// najpierw pętlą przejdziemy przez pierwszy wiersz
			string firstRow = "";
			string secondRow = "";
			string thirdRow = "";
			for (int i = 0; i < cells.Length; i++)
			{
				if (cells[i].GetRow() == 1)
				{
					firstRow += cells[i].GetGraphRep();
				}
				if (cells[i].GetRow() == 2)
				{
					secondRow += cells[i].GetGraphRep();
				}
				if (cells[i].GetRow() == 3)
				{
					thirdRow += cells[i].GetGraphRep();
				}
			}

			//Console.WriteLine(firstRow);
			//Console.WriteLine(secondRow);
			//Console.WriteLine(thirdRow);

			Console.Write(firstRow + Environment.NewLine + secondRow + Environment.NewLine + thirdRow);
			Console.ReadKey();
		}
       
    }
}
