﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    public enum CellBody  // przypisanie indeksów??
    {
        Empty = 0,
        X = 1,
        O = 10
    }

    public class Cell
    {
        public CellBody CellBody;
        private int row, column;
		private string graphRep;

        public Cell(int row, int column) //położenie komórek / obiekt klasy cell
        {
            this.row = row;
            this.column = column;

			if (row != 3 && column != 3)
			{
				graphRep = "_|";
			}
			if (row == 3 && column != 3)
			{
				graphRep = " |";
			}
			if (row != 3 && column == 3)
			{
				graphRep = "_";
			}
			if (row == 3 && column == 3)
			{
				graphRep = "";
			}
        }

		public string GetGraphRep()
		{
			return graphRep;
		}

        public int GetColumn()
        {
            return column;
        }

        public int GetRow()
        {
            return row;
        } 

    }
}
